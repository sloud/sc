sc() {
    sc_conf() {
      local config_file_name=".sc.json"
      local curr_dir="$PWD"

      while [[ "$curr_dir" != "/" ]]; do
        if [ -f "$curr_dir/$config_file_name" ]; then
            cat "$curr_dir/$config_file_name" | jq '.'
            return 0
        else
            curr_dir="$(dirname "$curr_dir")"
        fi
      done
    }

    sc_cmd_paths() {
        local conf="$(sc_conf)"

        echo "$conf" | jq -r 'path(.. | select(type=="string")) | join(".")'
    }

    sc_has_alias() {
        local cmd_path="$1"
        local conf="$(sc_conf)"

        echo "$conf" | jq -r ".$cmd_path | type==\"string\""
    }

    sc_alias_for_path() {
        local cmd_path="$1"
        local conf="$(sc_conf)"

        echo "$conf" | jq -r ".$cmd_path"
    }

    sc_split_path() {
        local cmd_path="$1"

        echo "$cmd_path" | sed 's/\./ /'
    }

    sc_join_path() {
        local cmd_path="$@"

        echo "$cmd_path" | sed 's/[ \t]\+/\./g'
    }

    sc_help() {
        for cmd_path in $(sc_cmd_paths); do
            echo "sc $(sc_split_path "$cmd_path")"
            echo "--> $(sc_alias_for_path "$cmd_path")"
            echo
        done
    }

    local cmd_path="$(sc_join_path $@)"
    local cmd="$(sc_alias_for_path "$cmd_path")"

    if [[ "$(sc_has_alias "$cmd_path")" == "true" ]]; then
        echo "--> $cmd"
        zsh -c "$cmd"
    else
        echo "--> options"
        echo $cmd
    fi
}

if [ ! -f "$HOME/.sc.json" ]; then

cat <<EOF > "$HOME/.sc.json"
{
    "foo": "echo \"foo\"",
    "bar": {
        "baz": "echo \"bar baz\""
    }
}
EOF

fi
